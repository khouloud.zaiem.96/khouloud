package Test;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.io.IOException;
import java.util.List;

public class Admin_UpdateUser extends STW {
@FindBy(id="list-users-search")
        WebElement Search;

    @FindBy(xpath="//*[@id='form_first_name']")
    WebElement FirstName;

    @FindBy(id="form_last_name")
    WebElement LastName;

    @FindBy(id="//span[@class='ui-checkbox']")
    WebElement Alias;
    @FindBy(id="form_user_alias")
    WebElement AliasValue;

    @FindBy(id="form_mobile_number")
    WebElement Number;

    @FindBy(id="form_email")
    WebElement Mail;

    @FindBy(id="form_position")
    WebElement Position ;
    @FindBy(id="call-fwd-tab")
    WebElement option;

    @FindBy(id="users_save")
    WebElement Next;
    @FindBy(xpath="//*[@id='allow-walkie-talkie-row']")
    WebElement PTT;
    @FindBy(id="select2-Priority-container")
    WebElement Priorite;

    @FindBy(id="allow-voip-video-row")
    WebElement videoCall;
    @FindBy(id="allow-video-streaming-row")
    WebElement VideoStreaming;
    @FindBy(xpath="//div[@class='row checkbox-tip flex-line']")
    WebElement OpStatus;
    @FindBy(id="change-geolocation-manager-row")
    WebElement Dispatch;


@FindBy(id="call-fwd-tab")
WebElement call_fwd;
    @FindBy(xpath="//div[@data-role='user-settings']/div/div")
    WebElement transfer;
    @FindBy(xpath="//div[@data-role='user-settings']/div/div[@class='customized-checkbox']")
    WebElement Custom;

    @FindBy(xpath="//div[@class='ui-dialog-titlebar ui-widget-header ui-helper-clearfix ui-draggable-handle']")
    WebElement dialog;
    @FindBy(xpath="/html/body/div[1]/div[1]/div[2]/div[2]/div[1]")
    WebElement error;
    @FindBy(id="activation-tab")
    WebElement Activation;
    @FindBy(xpath="//form/div[2]/div/div[7]/div[1]/div[1]/span")
    WebElement code;
    @FindBy(id="users_save")
    WebElement save;



    @FindBy(xpath="//div[@class='tokens-list']")
    WebElement codeActivation;

    @FindBy(xpath="//a[@class='menuTab-normal stw-main-menu-users']")
    WebElement UserTab;


    @FindBy(xpath="//div[@class='widget-row']")
    WebElement  userdetail;



    WebDriver driver;
    ExtentTest logger;
 //ExtentReportsManager extentReportsManager = new ExtentReportsManager(driver,logger);



    public Admin_UpdateUser(WebDriver driver, ExtentTest logger) {
        super(driver,logger);
        this.driver = driver;
        PageFactory.initElements(driver, this);
        this.logger = logger;

    }

 public void SerchContact(String profile) throws InterruptedException, IOException{
        Thread.sleep(5000);
       isElementDisplayed( UserTab);

     UserTab.click();
     isElementDisplayed( Search);
       Search.sendKeys(profile);

     logger.log(Status.INFO, profile+ " is entred  ");
     CheckDisplayedOfElement(userdetail,"Profile");
     userdetail.click();
     }








    public  void DeactiveTransfer(String profile) throws InterruptedException, IOException{
        SerchContact(profile);

        isElementDisplayed( option);
        option.click();
        isElementDisplayed( PTT);
        ActiveOptionRadio(PTT, "Push to talk ");
        DeactiveOptionRadio(VideoStreaming,"Video Streaming ");

        call_fwd.click();
        Thread.sleep(3000);

        scroll(transfer,driver);
        if (transfer.getAttribute("class").equals("checkbox-switcher on checked")) {
            System.out.println("cette option est deja activé ");
            logger.log(Status.INFO, "Call forward is alreday activated ");
        } else   {
            transfer.click();

        }


        save.click();

        Thread.sleep(3000);

        CheckDisplayedOfElement(dialog,"Confirmation required");

        WebElement Cancel =dialog.findElement(By.xpath(".//parent::*/div[2]/div[3]/div[1]/button"));
        WebElement Confirm =dialog.findElement(By.xpath(".//parent::*/div[2]/div[3]/div[2]/button"));


          CheckDisplayedOfElement(Cancel,"Cancel button");
        CheckDisplayedOfElement(Confirm,"Confirm button");
         Confirm.click();
        logger.log(Status.PASS, error.getText());


    }




    public void ValidationCode(String profile) throws IOException, InterruptedException{
        SerchContact(profile);
        isElementDisplayed(Activation);
        Activation.click();
        code.click();
        Next.click();
        dialog.findElement(By.xpath(".//parent::*/div[2]/div[3]/div[2]/button")).click();
        Thread.sleep(3000);
        if(isElementDisplayed(codeActivation)){
            System.out.println( codeActivation.getText());
            logger.log(Status.PASS,codeActivation.getText());
            //dialog.findElement(By.xpath("//div[2]/div[2]/div[2]/button")).click();

        }

    }
}
