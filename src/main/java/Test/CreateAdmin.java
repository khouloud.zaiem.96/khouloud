package Test;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.io.IOException;

public class CreateAdmin extends STW {

    WebDriver driver;
    ExtentTest logger;

    @FindBy(xpath="/html/body/div[1]/div[1]/section/aside/ul/li[3]")
    WebElement AdminAccount;
    @FindBy(xpath="/html/body/div[1]/div[1]/section/main/div[1]/div[2]/div[1]/a/button")
    WebElement Admin;
    @FindBy(xpath="//form/div/div/div/div[1]/div[1]/div/div[1]/input")
    WebElement mail;
    @FindBy(xpath="//form/div/div/div/div[1]/div[2]/div/div[1]/input")
    WebElement FirstName;
    @FindBy(xpath="//form/div/div/div/div[1]/div[3]/div/div[1]/input")
    WebElement LastName;
    @FindBy(xpath="//button[contains(text(), 'Add')]")
     WebElement Add;

    @FindBy(xpath="/html/body/div/div[1]/section/main/div[1]/div[2]/div[1]/div/div/input")
    WebElement searchAdmin;

    @FindBy(xpath="//span[text()='No Data']")
    WebElement noData;

    @FindBy(xpath="//div[@class='el-notification right']")
    WebElement error;
    @FindBy(xpath="//table[@class='el-table__body']/tbody/tr/td[5]/div/a")
    WebElement editBtn;

    @FindBy(xpath="//form/div[1]/div/div/div[1]/div[4]/div/div/input")
    WebElement mobilenumber;
    @FindBy(xpath="//button[contains(text(), 'Save')]")
    WebElement save;
    @FindBy(xpath="/html/body/div[2]/p")
    WebElement succes;

    @FindBy(xpath="//table[@class='el-table__body']/tbody/tr/td[5]/div/button[2]")
    WebElement deleteBtn;

    @FindBy(xpath="//div[@role='dialog']")
    WebElement dialog;

    @FindBy(xpath="/div/div[3]/button[1]")
    WebElement cancel;


    public CreateAdmin(WebDriver driver, ExtentTest logger) {
        super(driver,logger);
        this.driver = driver;
        PageFactory.initElements(driver, this);
        this.logger = logger;
    }



    public void creatAdmin(String email,String firstName,String lastname) throws InterruptedException, IOException{
AdminAccount.click();
Admin.click();
    mail.sendKeys(email);
    FirstName.sendKeys(firstName);
    LastName.sendKeys(lastname);
    Thread.sleep(2000);
    Add.click();
        if (isElementDisplayed(error)){
            System.out.println(error.getText());
            logger.log(Status.PASS, error.getText());
           getscreenshot();
        }

        else{
            logger.log(Status.FAIL, "Admin is Created  with sucess ");

        }
    }


    public void SearchAdmin(String name) throws InterruptedException, IOException{
        AdminAccount.click();
        searchAdmin.sendKeys(name);
        searchAdmin.sendKeys(Keys.RETURN);

        if (isElementDisplayed(error) || isElementDisplayed(noData)){

            if (isElementDisplayed(error)) {
                logger.log(Status.FAIL, error.getText());}

            if (isElementDisplayed(noData)){
                logger.log(Status.FAIL, "No Data");}

            getscreenshot();
        }
        else{
            logger.log(Status.PASS, "Admin is displayed");


        }
    }
    public void EditAdmin(String name, String phonenumber) throws InterruptedException, IOException{
        SearchAdmin(name);
        CheckDisplayedOfElement(editBtn,"Edit button of Admin");
        editBtn.click();
        mobilenumber.clear();
        mobilenumber.sendKeys(phonenumber);
        save.click();

            logger.log(Status.PASS, succes.getText());

    }


    public void DeletAdmin(String name) throws InterruptedException, IOException{
        SearchAdmin(name);
        CheckDisplayedOfElement(deleteBtn,"Delet button of Admin");
        deleteBtn.click();
        Thread.sleep(2000);

        CheckDisplayedOfElement(dialog,"You are about to delete this account. Are you sure?  ");

        WebElement cancel =dialog.findElement(By.xpath("//div/div[3]/button[1]"));
        WebElement OK =dialog.findElement(By.xpath("//div/div[3]/button[2]"));

        CheckDisplayedOfElement(cancel ,"Cancel button   ");
        CheckDisplayedOfElement(OK ,"OK button   ");

        OK.click();
        logger.log(Status.PASS, succes.getText());
    }
}
