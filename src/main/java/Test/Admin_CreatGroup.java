package Test;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.io.IOException;
import java.util.List;

public class Admin_CreatGroup extends  STW{

@FindBy(xpath="//a[@class='menuTab-normal stw-main-menu-groups']")
    WebElement Groupe;

@FindBy(id="group-create")
        WebElement CreatBtn;
@FindBy(id="group-name")
        WebElement GroupName;
@FindBy(xpath="//div[@class='search-bar']/input")
        WebElement SearchUser;
@FindBy(xpath="//div[@class='assign-arrow']")
        WebElement assign_arrow;
@FindBy(id="next-button")
        WebElement next ;


    @FindBy(id="save-button")
    WebElement save ;
    @FindBy(xpath="//div[@class='company-widgets-notifications-area']")
    WebElement error;
    @FindBy(id="search-group")
            WebElement searchGp;
    @FindBy(xpath="//button[@class='trash-button enabled']")
            WebElement DeleteBtn;
    @FindBy(xpath="//div[@class='ui-dialog ui-widget ui-widget-content ui-front ui-draggable']")
    WebElement dialog;
    @FindBy(id="group-edit")
            WebElement EditBtn;

    WebDriver driver;
    ExtentTest logger;
  //  ExtentReportsManager extentReportsManager = new ExtentReportsManager(driver,logger);



    public Admin_CreatGroup(WebDriver driver, ExtentTest logger) {

        super(driver,logger);
        this.driver = driver;
        PageFactory.initElements(driver, this);
        this.logger = logger;
    }


    public void CreateGroup(String groupName, String user1 , String user2) throws InterruptedException, IOException{
        Thread.sleep(3000);
        isElementDisplayed(Groupe);

        Groupe.click();
        isElementDisplayed(CreatBtn);
        CheckDisplayedOfElement(CreatBtn,"Create Button");
        CreatBtn.click();
        GroupName.sendKeys(groupName);
        AddUser(user1);
       assign_arrow.click();
        AddUser(user2);
        next.click();
        save.click();

        if (error.getText().contains("Please choose a different name.")){
            CheckDisplayedOfElement(error,"Error message ");
            System.out.println(error.getText());
            logger.log(Status.FAIL, error.getText());
           getscreenshot();
        }
        else{
            CheckDisplayedOfElement(error,"Sucess message ");
            logger.log(Status.PASS, error.getText());

        }

    }




    public void AddUser(String Name) throws InterruptedException, IOException{
        SearchUser.clear();
        SearchUser.sendKeys(Name);
        SearchUser.sendKeys(Keys.RETURN);
        Thread.sleep(3000);
        List<WebElement> li_All=driver.findElements(By.xpath("//div[@class='name-block']/label"));
        for (WebElement element : li_All) {
            if (element.getText().equals(Name)) {
                String cheked = element.findElement(By.xpath(".//parent::*//parent::*//div/div/span")).getAttribute("class");
                System.out.println("element est "+cheked);
                if (cheked.equals("unchecked stw-unchecked")){

                    logger.log(Status.FAIL, "user "+Name+" is already cheked ");
                    getscreenshot();
                }else {



                    element.click();
                    logger.log(Status.PASS, "user "+Name+" is  cheked ");
                    assign_arrow.click();
            }

                break;
            }

        }

    }

    public void Search(String parametre) throws InterruptedException, IOException{
        Thread.sleep(5000);
        isElementDisplayed(Groupe);
        Groupe.click();
        searchGp.sendKeys(parametre);
        Thread.sleep(3000);
        List<WebElement> li_All=driver.findElements(By.xpath("//div[@class='group-item-name-container']/div/span"));

        for (WebElement element : li_All) {
            System.out.println("element est "+element.getText());
            if (element.getText().equals(parametre)) {
                element.findElement(By.xpath(".//parent::*//parent::*")).click();
                logger.log(Status.PASS, "Group is displayed ");
                break;

            }
            else{
                logger.log(Status.FAIL, "Group is not displayed ");
                getscreenshot();

            }
        }
}

    public void Delete(String parametre) throws InterruptedException, IOException{
        Search(parametre);
        CheckDisplayedOfElement(DeleteBtn,"Delete button");
        DeleteBtn.click();
        isElementDisplayed( dialog.findElement(By.xpath("//div[3]/div[2]/button")));
        CheckDisplayedOfElement(dialog,dialog.getText());
        System.out.println("element est " +dialog.getText());
        Thread.sleep(3000);
        dialog.findElement(By.xpath(".//div[3]/div[2]/button")).click();
        CheckDisplayedOfElement(error,"Success message ");
        logger.log(Status.PASS, error.getText());
}



    public void EditGroup(String parametre,String user) throws InterruptedException, IOException{
        Search(parametre);
        EditBtn.click();
        AddUser(user);
        next.click();
        save.click();
        Thread.sleep(3000);
        dialog.findElement(By.xpath(".//div[3]/div[2]/button")).click();

        if (error.getText().contains("Please choose a different name.")){
            CheckDisplayedOfElement(error,"Error message ");
            System.out.println(error.getText());
            logger.log(Status.FAIL, error.getText());
            getscreenshot();
        }
        else{
            CheckDisplayedOfElement(error,"Success message ");
            logger.log(Status.PASS, error.getText());

        }
}

}
