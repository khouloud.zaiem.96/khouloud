package Test;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.IOException;
import java.util.List;

public class Admin_CreateUSer extends STW{
    @FindBy(xpath="//*[@id='form_first_name']")
    WebElement FirstName;

    @FindBy(id="form_last_name")
    WebElement LastName;

    @FindBy(id="//span[@class='ui-checkbox']")
    WebElement Alias;
    @FindBy(id="form_user_alias")
    WebElement AliasValue;

    @FindBy(id="form_mobile_number")
    WebElement Number;

    @FindBy(id="form_email")
    WebElement Mail;

    @FindBy(id="form_position")
    WebElement Position ;


    @FindBy(id="users_save")
    WebElement Next;

    @FindBy(id="allow-walkie-talkie-row")
    WebElement PTT;
    @FindBy(id="select2-Priority-container")
    WebElement Priorite;

    @FindBy(id="allow-voip-video-row")
    WebElement videoCall;

    @FindBy(xpath="//div[@class='row checkbox-tip flex-line']")
    WebElement OpStatus;
    @FindBy(id="change-geolocation-manager-row")
    WebElement Dispatch;

    @FindBy(id="allow-video-streaming-row")
    WebElement VideoStreaming;
    @FindBy(xpath="//div[@data-role='user-settings']/div/div")
    WebElement transfer;


    
    @FindBy(xpath="//div[@data-role='user-settings']/div/div[@class='customized-checkbox']")
    WebElement Custom;

    @FindBy(xpath="//div[@role='dialog']")
    WebElement dialog;
    @FindBy(xpath="/html/body/div[1]/div[1]/div[2]/div[2]/div[1]")
    WebElement error;


    @FindBy(xpath="//div[@class='widget-notification-message']")
    WebElement notif;

    @FindBy(xpath="//div[@class='tokens-list']")
    WebElement codeActivation;



    WebDriver driver;
    ExtentTest logger;



    public Admin_CreateUSer(WebDriver driver,     ExtentTest logger) {
        super(driver,logger);
        this.driver = driver;
        PageFactory.initElements(driver, this);
        this.logger = logger;
    }

    public void CheckElement() throws IOException{

        CheckDisplayedOfElement(FirstName,"First Name");
        CheckDisplayedOfElement(LastName,"Lirst Name");
        CheckDisplayedOfElement(Alias,"Alias");
        CheckDisplayedOfElement(Number,"Mobile Number");
        CheckDisplayedOfElement(Mail,"E-mail Address");
        CheckDisplayedOfElement(Position,"Position");

    }
    public void option(String FName,String LName , String number) throws InterruptedException, IOException{
        isElementDisplayed(FirstName);
        Thread.sleep(6000);
        FirstName.sendKeys(FName);
        LastName.sendKeys(LName);
       // Alias.click();
        //AliasValue.sendKeys(Aliasvalue);
        Number.sendKeys(number);
        Mail.sendKeys("test.team.sw@gmail.com");
        Position.sendKeys("QA");

        logger.log(Status.INFO, "User details are created ") ;

        Thread.sleep(3000);
        isElementDisplayed(Next);
        Next.click();
        PTT.click();
        Thread.sleep(3000);
        Priorite.click();
        Thread.sleep(3000);
         SelectScroll("Level 252");
        WebDriverWait wait = new WebDriverWait(driver, 2);
        wait.until((ExpectedConditions.elementToBeClickable(videoCall)));
        videoCall.click();
        VideoStreaming.click();
       // OpStatus.click();
        Dispatch.click();

        logger.log(Status.INFO, "User Option are selected ") ;
        Next.click();



    }



    public void SelectScroll(String Parametre){
        List<WebElement> li_All=driver.findElements(By.xpath("//ul[@class='select2-results__options']/li"));
        // System.out.println(li_All.size());
        for (WebElement element : li_All) {
            System.out.println("element est "+element.getText());
            if (element.getText().equals(Parametre)) {
                scroll(element, driver);
                element.click();
                break;
            }
        }
    }



    public void TransferCallToContact(String status,String parametre) throws InterruptedException{
        transfer.click();
        Custom.click();

        List<WebElement> li_All=driver.findElements(By.xpath("//div[@class='row-with-switcher']/label"));
        for (WebElement element : li_All) {
            System.out.println("element est " + element.getText());
            if (element.getText().equals(status)) {
                WebElement parent=element.findElement(By.xpath(".//parent::*//parent::*"));
                parent.findElement(By.xpath(".//div//div")).click();

                logger.log(Status.INFO,status + " is selected ") ;

                Thread.sleep(3000);

                parent.findElement(By.xpath(".//div[2]//div[2]")).click();

                Thread.sleep(3000);

                List<WebElement> contact=parent.findElements(By.xpath("//ul[@id='dropdownForinternal-number-selector']/li/a"));
                // System.out.println(li_All.size());
                for (WebElement elementContact : contact) {
                    System.out.println("elementContact est "+elementContact.getText());
                    if (elementContact.getText().equals(parametre)) {
                        scroll(elementContact, driver);
                        elementContact.click();
                        logger.log(Status.INFO,parametre+ " is selected ");
                        break;
                    }
                }



           break;
            } else {

                System.out.println(status + "not displayed");
            }

        }
    }




    public void TransferCallToExternal(String status,String parametre) throws InterruptedException{

        List<WebElement> li_All=driver.findElements(By.xpath("//div[@class='row-with-switcher']/label"));
        for (WebElement element : li_All) {
            System.out.println("element est " + element.getText());
            if (element.getText().equals(status)) {
                WebElement parent=element.findElement(By.xpath(".//parent::*//parent::*"));
                parent.findElement(By.xpath(".//div//div")).click();
                logger.log(Status.INFO,status + " is selected ") ;

                Thread.sleep(3000);

                parent.findElement(By.xpath(".//div[2]//div[1]")).click();
                Thread.sleep(3000);

                List<WebElement> contact=parent.findElements(By.xpath("//ul[@class='ui-dropdown-options ui-corner-bottom']/li/a"));
                // System.out.println(li_All.size());
                for (WebElement elementContact : contact) {
                    System.out.println("Status est "+elementContact.getText());
                    if (elementContact.getText().equals("External number")) {
                        scroll(elementContact, driver);
                        elementContact.click();
                        break;
                    }
                }

                parent.findElement(By.xpath(".//input[@data-role='external-number']")).sendKeys(parametre);
                logger.log(Status.INFO, parametre+" is the external call  ") ;
                break;


            }
            else {
                //logger.log(Status.FAIL,status + "not displayed");
                System.out.println(status + "not displayed");
            }

        }

    }

    public void callBarring(String parameter, String Profile){
        List<WebElement> li_All=driver.findElements(By.xpath("//div[@class='row']/label[@class='label']"));
        for (WebElement row : li_All) {
            System.out.println("element est " + row.getText());
            if (row.getText().contains(parameter)) {
                System.out.println("element est " + row.getText());
                WebElement parent=row.findElement(By.xpath(".//parent::*"));
                parent.findElement(By.xpath(".//div")).click();

                if (row.getText().equals("Custom profile")) {
                    driver.findElement(By.xpath("//span[@id='select2-UserCallBarringProfile-container']")).click();

                    List<WebElement> contact=parent.findElements(By.xpath("//ul[@class='select2-results__options']/li"));
                    // System.out.println(li_All.size());
                    for (WebElement elementContact : contact) {
                        System.out.println("Profile est " + elementContact.getText());
                        if (elementContact.getText().equals(Profile)) {
                            scroll(elementContact, driver);
                            elementContact.click();
                            logger.log(Status.INFO,Profile+ "profile  is selected ") ;

                            break;
                        }
                    }

                }
            } else {

                System.out.println(parameter + "not displayed");
            }
        }
    }


    public void InvalideUser() throws IOException, InterruptedException{
        isElementDisplayed(FirstName);
        Thread.sleep(6000);
        Next.click();
        Next.click();
        Next.click();
        Next.click();
        CheckNotDisplayedOfElement(Next,"Create button ");
    }

    public void UserError(String FName,String LName , String number) throws IOException, InterruptedException{
        option(FName,LName,number);
        Next.click();
        Next.click();
        Next.click();
        if (isElementDisplayed(notif)){
            logger.log(Status.PASS, notif.getText());
        }

        getscreenshot();
    }




    public void Simpleuer(String FName,String LName , String number) throws IOException, InterruptedException{
        option(FName,LName,number);
        Next.click();
        Next.click();
        Next.click();
        validateuser();
    }

    public void USerWithforwardexternal(String FName,String LName , String number, String contact) throws InterruptedException, IOException{
        option(FName,LName,number);
        TransferCallToExternal("Always",contact);
        Next.click();
        Next.click();
        Next.click();
        if (isElementDisplayed(notif)){
            logger.log(Status.PASS, notif.getText());
        }
        validateuser();


    }


    public void USerWithforwardcontact(String FName,String LName , String number, String contact) throws InterruptedException, IOException{
        option(FName,LName,number);

        transfer.click();
        Custom.click();

        TransferCallToContact("Always",contact);
        Next.click();
        Next.click();
        Next.click();
        if (isElementDisplayed(notif)){
            logger.log(Status.PASS, notif.getText());
            getscreenshot();
        }
        validateuser();
    }

    public void USerWithBarring(String FName,String LName , String number, String Profile) throws IOException, InterruptedException{
        option(FName,LName,number);
        Next.click();
        callBarring("Custom profile",Profile);
        Next.click();
        Next.click();
        if (isElementDisplayed(notif)){
            logger.log(Status.PASS, notif.getText());
            getscreenshot();
        }
        validateuser();

    }



    public void CreatValideUser(String FName,String LName , String number,  String InternalContact,String contactNumber,String Profile) throws IOException, InterruptedException{
        option(FName,LName,number);

        TransferCallToContact("When Unreachable",InternalContact);

        TransferCallToExternal("When Busy",contactNumber);
        Next.click();
        callBarring("Custom profile",Profile);
        Next.click();
        Next.click();

        if (isElementDisplayed(notif)){
            logger.log(Status.FAIL, notif.getText());
            getscreenshot();
        }
        validateuser();
    }







    public void validateuser() throws IOException{


        if(isElementDisplayed(codeActivation)){
            System.out.println( codeActivation.getText());
            logger.log(Status.PASS,codeActivation.getText());
            //dialog.findElement(By.xpath("//div[2]/div[2]/div[2]/button")).click();

        }

        else {
         //   logger.log(Status.FAIL, notif.getText());
         //getscreenshot();

        }

    }
}


