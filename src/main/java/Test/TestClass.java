package Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.*;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

public class TestClass {
    WebDriver driver;

    LoginScreen loging;
    CreateAdmin account;
    ConnectAdmin connectAdmin;
    CreateCompany organization;
    ExtentReportsManager extentReportsManager;
   ExtentReports report;
    ExtentTest logger;
    CreateGroupe creategroupe;
    CreatUser creatuser;
    Admin_CreateUSer Admin_CreatUSer;
    CreateResellers resellers;
    Admin_UpdateUser Admin_UpdateUSer;
    Admin_CreatGroup AdminCreateGroup;
    Settings settings;
    CreateDepartment department;
    MyBusniess myBusniess;

    @BeforeSuite
    public  void startTest() throws IOException{
    extentReportsManager = new ExtentReportsManager();
     report  =ExtentReportsManager.getReportInstances();

    }

@AfterSuite
        public void End (){
        driver.quit();
        report.flush();}

@Test
public void Navigationplatforme() throws IOException{

        System.setProperty("webdriver.gecko.driver", "/Users/macbookpro/IdeaProjects/SeleniumProject/geckodriver");
        driver=new FirefoxDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.get("https://rh8-superadmin-preprod.teamontherun.com/#/login");

    }


    @Test(priority = 1)

    void Login_STC1() throws InterruptedException, IOException{


       logger=report.createTest("Login_STC1","Check disponiblite of element ");
        loging=new LoginScreen(driver, logger);
        Navigationplatforme();
        loging.GetUseCaseDescription("Login_STC1",0,logger);
        loging.checkelement();

    }


    @Parameters({"WrongPassword","WrongLogin"})
    @Test(priority = 2)
    void Login_STC2(String WrongPassword, String WrongLogin) throws InterruptedException, IOException{

        logger=report.createTest("Login_STC2","Check error ");
        loging=new LoginScreen(driver, logger);
        Navigationplatforme();
        loging.GetUseCaseDescription("Login_STC2",0, logger);
        loging.connect(WrongPassword,WrongLogin );

    }


    @Parameters({"Password","Login"})
    @Test(priority = 3)
   public void Login_STC3(String Password,String Login) throws InterruptedException, IOException{
       logger=report.createTest("Login_STC3","login with sucess  ");
        loging=new LoginScreen(driver, logger);
        loging.GetUseCaseDescription("Login_STC3",0, logger);
        loging.connect(Password, Login);

    }

    // Acount

    @Parameters({"Password","Login","email","FirstName","LastNAme"})
    @Test(priority = 4)
    void Account_STC1(String Password,String Login,String email ,String FirstName, String LastNAme) throws InterruptedException, IOException{
        logger=report.createTest("Account_STC1","Add Acount existant and check error");
        Navigationplatforme();
        account=new CreateAdmin(driver, logger);
        loging=new LoginScreen(driver, logger);
        loging.connect(Password,Login);
        account.GetUseCaseDescription("Account_STC1",1, logger);
        account.creatAdmin(email,FirstName,LastNAme);

    }

    @Parameters({"email","FirstName","LastNAme"})
    @Test(priority = 5)
    void Account_STC2(String email ,String FirstName, String LastNAme) throws InterruptedException, IOException{
        logger=report.createTest("Account_STC2","Add Admin Account ");
        account=new CreateAdmin(driver, logger);
        account.GetUseCaseDescription("Account_STC2",1, logger);
        account.creatAdmin(email,FirstName,LastNAme);
    }

    @Parameters({"NameAdmin"})
    @Test(priority = 6)
    void Account_STC3(String NameAdmin) throws InterruptedException, IOException{
        logger=report.createTest("Account_STC3","Search  Admin Account ");
        account=new CreateAdmin(driver, logger);
        account.GetUseCaseDescription("Account_STC3",1, logger);
        account.SearchAdmin(NameAdmin);
    }

    @Parameters({"NameAdmin","phoneNumber"})
    @Test(priority = 7)
    void Account_STC4(String Name,String phoneNumer) throws InterruptedException, IOException{
        logger=report.createTest("Account_STC4","Edit Admin  Account ");
        account=new CreateAdmin(driver, logger);
        account.GetUseCaseDescription("Account_STC4",1, logger);
        account.EditAdmin(Name , phoneNumer);
    }

    @Parameters({"NameAdmin"})
    @Test(priority = 8)
    void Account_STC5(String NameAdmin) throws InterruptedException, IOException{
        logger=report.createTest("Account_STC5","Delet  Admin  Account ");
        account=new CreateAdmin(driver, logger);
        account.GetUseCaseDescription("Account_STC5",1, logger);
        account.DeletAdmin(NameAdmin);
    }





//Resellers
    @Test(priority = 8)
    void Resellers_STC1() throws InterruptedException, IOException{
        logger=report.createTest("Resellers_STC1","Ckeck Elements of Resellers ");
        resellers=new CreateResellers(driver, logger);
        resellers.GetUseCaseDescription("Resellers_STC1",2, logger);
        resellers.checkelement();
    }


    @Parameters({"ResselersName", "billingValue", "cityvalue"," zipcodevalue", "countryvalue",
            "FNRE",  "LNRE", "PNRE", "ERE",
            "FNRS", "LNRS", "PNRS", "ERS",
            "FNSC",  "LNSC", "PNSC", "ESC",
            "FNSA",  "LNSA", "PNSA", "ESA"})
    @Test(priority = 9)
    void Resellers_STC2(String nameResselers,String billingValue,String cityvalue,String zipcodevalue,String countryvalue,
                        String FNRE, String LNRE,String PNRE,String ERE,
                        String FNRS,String LNRS,String PNRS,String ERS,
                        String FNSC, String LNSC,String PNSC,String ESC,
                        String FNSA, String LNSA,String PNSA,String ESA) throws InterruptedException, IOException{
        logger=report.createTest("Resellers_STC2","Add Resellers ");
        resellers=new CreateResellers(driver, logger);
        resellers.GetUseCaseDescription("Resellers_STC2",2, logger);

        resellers.create( nameResselers, billingValue, cityvalue, zipcodevalue, countryvalue,
                 FNRE,  LNRE, PNRE, ERE,
                 FNRS, LNRS, PNRS, ERS,
                 FNSC,  LNSC, PNSC, ESC,
                 FNSA,  LNSA, PNSA, ESA);
    }

    @Parameters({"ResselersName"})
    @Test(priority = 10)
    void Resellers_STC3(String ResselersName) throws InterruptedException, IOException{
        logger=report.createTest("Account_STC3","Search  Resellers ");
        resellers=new CreateResellers(driver, logger);
        resellers.Search(ResselersName);
    }

    @Parameters({"ResselersName"})
    @Test(priority = 11)
    void Resellers_STC4(String ResselersName) throws InterruptedException, IOException{
        logger=report.createTest("Account_STC4","Edit   Resellers ");
        resellers=new CreateResellers(driver, logger);
        resellers.editResellers(ResselersName);
    }

    @Parameters({"ResselersName"})
    @Test(priority = 12)
    void Resellers_STC5(String ResselersName) throws InterruptedException, IOException{
        logger=report.createTest("Account_STC5","Delet  Resellers ");
        resellers=new CreateResellers(driver, logger);
        resellers.deletResellers(ResselersName);
    }

//Organization
    @Test(priority = 13)
     void Organization_STC1() throws IOException{
        logger=report.createTest("Organization_STC1","Check disponiblite of element  ");
        organization =new CreateCompany(driver, logger);
        organization.GetUseCaseDescription("Organization_STC1",3, logger);
        organization.checkelement();
    }

    @Test(priority = 14)
    void Organization_STC2() throws InterruptedException, IOException{
        logger=report.createTest("Organization_STC2","Check error message ");
        organization =new CreateCompany(driver, logger);
        organization.GetUseCaseDescription("Organization_STC3",3, logger);
        organization.OrganizationTab();
        organization.AddCompany("","None","","","",505,190);
    }


    @Parameters({"OrgName","ResName","FirstName","LastNAme","email","nbUser","nbGroup"})
    @Test(priority = 15)
    void Organization_STC3(String companyName, String resealler, String firstName, String lastName,String email,int nbUser ,int nbGroup) throws InterruptedException, IOException{
        logger=report.createTest("Organization_STC3","Add Organization");
        organization =new CreateCompany(driver, logger);
        organization.GetUseCaseDescription("Organization_STC3",3, logger);
        organization.AddCompany(companyName,resealler,firstName,lastName,email,nbUser,nbGroup);

    }

    @Parameters({"companyName"})
    @Test(priority = 16)
    void Organization_STC4(String companyName ) throws InterruptedException, IOException{
        logger=report.createTest("Organization_STC4","Organization search functionality");
        organization =new CreateCompany(driver, logger);
        organization.GetUseCaseDescription("Organization_STC4",3, logger);
        organization.Search(companyName);
    }


    @Parameters({"companyName"})
    @Test(priority = 17)
    void Organization_STC5(String companyName) throws InterruptedException, IOException{
        logger=report.createTest("Organization_STC5","Edit Organization");
        organization =new CreateCompany(driver, logger);
        organization.GetUseCaseDescription("Organization_STC5",3, logger);
        organization.editCompany(companyName);
    }


    @Parameters({"companyName"})
    @Test(priority = 18)
    void Organization_STC6(String companyName) throws InterruptedException, IOException{
        logger=report.createTest("Organization_STC6","Delete Organization");
        organization =new CreateCompany(driver, logger);
        organization.GetUseCaseDescription("Organization_STC6",3, logger);
        organization.editCompany(companyName);
    }


//departement
    @Test(priority = 18)
    void Organization_STC7() throws InterruptedException, IOException{
        logger=report.createTest("Organization_STC7","Export CSV");
        department =new CreateDepartment(driver, logger);

    }

    @Parameters({"companyName"})
    @Test(priority = 19)
    void Organization_STC8(String companyName) throws InterruptedException, IOException{
        logger=report.createTest("Organization_STC8","Select Organzation functionality");
        department =new CreateDepartment(driver, logger);
        department.GetUseCaseDescription("Organization_STC8",3, logger);
        department.SelectORG(companyName);
    }

    @Parameters({"companyName","DepartmentName"})
    @Test(priority = 19)
    void Organization_STC9(String companyName,String Department) throws InterruptedException, IOException{
        logger=report.createTest("Organization_STC9","Department search functionality");
        department =new CreateDepartment(driver, logger);
        department.GetUseCaseDescription("Organization_STC9",3, logger);
        department.searchDepartment(companyName,Department);
    }


    @Parameters({"companyName","DepartmentName","level"})
    @Test(priority = 19)
    void Organization_STC10(String companyName,String Department,String level) throws InterruptedException, IOException{
        logger=report.createTest("Organization_STC10","Add Department");
        department =new CreateDepartment(driver, logger);
        department.GetUseCaseDescription("Organization_STC10",3, logger);
        department.CreatDepartment(companyName,Department,level);
    }

    @Parameters({"companyName","DepartmentName"})
    @Test(priority = 19)
    void Organization_STC11(String companyName,String Department) throws InterruptedException, IOException{
        logger=report.createTest("Organization_STC11","Edit  Department");
        department =new CreateDepartment(driver, logger);
        department.GetUseCaseDescription("Organization_STC11",3, logger);
        department.EditDepartment(companyName,Department);
    }

    @Parameters({"companyName","DepartmentName"})
    @Test(priority = 19)
    void Organization_STC12(String companyName,String Department) throws InterruptedException, IOException{
        logger=report.createTest("Organization_STC12","Delet Department");
        department =new CreateDepartment(driver, logger);
        department.GetUseCaseDescription("Organization_STC12",3, logger);
        department.deletDepartment(companyName,Department);
    }


    @Parameters({"companyName","GroupName"})
    @Test(priority = 19)
    void Organization_STC13(String companyName,String GroupName) throws InterruptedException, IOException{
        logger=report.createTest("Organization_STC13","Groups search functionality");
        creategroupe =new CreateGroupe(driver, logger);
        creategroupe.GetUseCaseDescription("Organization_STC13",3, logger);
        creategroupe.Searchgroup(companyName,GroupName);
    }


    @Parameters({"companyName","GroupName"})
    @Test(priority = 19)
    void Organization_STC14(String companyName,String GroupName) throws InterruptedException, IOException{
        logger=report.createTest("Organization_STC14","Mulitple selection functionality");
        creategroupe =new CreateGroupe(driver, logger);
        creategroupe.GetUseCaseDescription("Organization_STC14",3, logger);
        creategroupe.SelectGroups(companyName,GroupName);
    }


    @Parameters({"companyName","GroupName","DepartmentName","Level"})
    @Test(priority = 19)
    void Organization_STC15(String companyName,String GroupName, String Department,String level) throws InterruptedException, IOException{
        logger=report.createTest("Organization_STC15","Add groups");
        creategroupe =new CreateGroupe(driver, logger);
        creategroupe.GetUseCaseDescription("Organization_STC15",3, logger);
        creategroupe.CreatGroupe(companyName,GroupName,Department,level);
    }

    @Parameters({"companyName","GroupName","DepartmentName"})
    @Test(priority = 19)
    void Organization_STC16(String companyName,String GroupName, String Department,String level) throws InterruptedException, IOException{
        logger=report.createTest("Organization_STC16","Add groups");
        creategroupe =new CreateGroupe(driver, logger);
        creategroupe.GetUseCaseDescription("Organization_STC15",3, logger);
        creategroupe.CreatGroupe(companyName,GroupName,Department,level);
    }


    @Parameters({"companyName","GroupName"})
    @Test(priority = 19)
    void Organization_STC17(String companyName,String GroupName) throws InterruptedException, IOException{
        logger=report.createTest("Organization_STC17","Edit groups");
        creategroupe =new CreateGroupe(driver, logger);
        creategroupe.GetUseCaseDescription("Organization_STC15",3, logger);
        creategroupe.Editgroup(companyName,GroupName);
    }

    @Parameters({"companyName","GroupName"})
    @Test(priority = 19)
    void Organization_STC18(String companyName,String GroupName) throws InterruptedException, IOException{
        logger=report.createTest("Organization_STC18","Delet groups");
        creategroupe =new CreateGroupe(driver, logger);
        creategroupe.GetUseCaseDescription("Organization_STC19",3, logger);
        creategroupe.deletgroup(companyName,GroupName);
    }


    @Parameters({"companyName"})
    @Test(priority = 19)
    void LoginAdmin_STC1(String companyName) throws InterruptedException, IOException{
        logger=report.createTest("LoginAdmin_STC1","Check Connect button");
        connectAdmin =new ConnectAdmin(driver, logger);
        connectAdmin.GetUseCaseDescription("LoginAdmin_STC1",5, logger);
        connectAdmin.checkElement(companyName);
    }


    @Parameters({"companyName"})
    @Test(priority = 19)
    void LoginAdmin_STC2(String companyName) throws InterruptedException, IOException{
        logger=report.createTest("LoginAdmin_STC2","Connect Admin");
        connectAdmin =new ConnectAdmin(driver, logger);
        connectAdmin.GetUseCaseDescription("LoginAdmin_STC2",5, logger);
        connectAdmin.ConnectAdmin(companyName);
    }




    @Test(priority = 19)
    void User_STC1() throws InterruptedException, IOException{
        logger=report.createTest("User_STC1","Create user");
        creatuser =new CreatUser(driver, logger);
        creatuser.GetUseCaseDescription("User_STC1",6, logger);
        creatuser.CreateUser("+216","21222324","KHOUKOU","zaiem","TnTesting");
    }


    @Parameters({"UserName"})
    @Test(priority = 19)
    void User_STC2(String UserName) throws InterruptedException, IOException{
        logger=report.createTest("User_STC2","Connect Admin");
        creatuser =new CreatUser(driver, logger);
        creatuser.GetUseCaseDescription("User_STC2",6, logger);
        creatuser.SearchUser(UserName);
    }


    @Parameters({"UserName"})
    @Test(priority = 19)
    void User_STC3(String UserName) throws InterruptedException, IOException{
        logger=report.createTest("User_STC3","Connect Admin");
        creatuser =new CreatUser(driver, logger);
        creatuser.GetUseCaseDescription("User_STC3",6, logger);
        creatuser.Updateuser(UserName);
    }

    @Parameters({"UserName"})
    @Test(priority = 19)
    void User_STC4(String UserName) throws InterruptedException, IOException{
        logger=report.createTest("User_STC4","Connect Admin");
        creatuser =new CreatUser(driver, logger);
        creatuser.GetUseCaseDescription("User_STC4",6, logger);
        creatuser.deleteuser(UserName);
    }


    @Test(priority = 20)
    void AdminUser_STC1() throws InterruptedException, IOException{
        logger=report.createTest("User_STC4","Chek elements");
        Admin_CreatUSer =new Admin_CreateUSer(driver, logger);
        Admin_CreatUSer.GetUseCaseDescription("User_STC4",7, logger);
        Admin_CreatUSer.CheckElement();
    }

    @Test(priority = 20)
    void AdminUser_STC2() throws InterruptedException, IOException{
        logger=report.createTest("User_STC2","Create Invalide User");
        Admin_CreatUSer =new Admin_CreateUSer(driver, logger);
        Admin_CreatUSer.GetUseCaseDescription("User_STC2",7, logger);
        Admin_CreatUSer.InvalideUser();
    }


    @Test(priority = 20)
    void AdminUser_STC3() throws InterruptedException, IOException{
        logger=report.createTest("User_STC2","Connect Admin");
        Admin_CreatUSer =new Admin_CreateUSer(driver, logger);
        Admin_CreatUSer.GetUseCaseDescription("User_STC2",7, logger);
        Admin_CreatUSer.UserError("kkk", "zzaa","444444444" );
    }


    @Parameters({"LastNAme","FirstName","phoneNumber"})
    @Test(priority = 20)
    void AdminUser_STC4(String LastNAme,String FirstName , String phoneNumber) throws InterruptedException, IOException{
        logger=report.createTest("User_STC4", "Connect Admin");
        Admin_CreatUSer=new Admin_CreateUSer(driver, logger);
        Admin_CreatUSer.GetUseCaseDescription("User_STC4", 7, logger);
        Admin_CreatUSer.Simpleuer(LastNAme,FirstName,phoneNumber);
    }


    @Parameters({"LastNAme","FirstName","phoneNumber","ExternalContact"})
    @Test(priority = 20)
    void AdminUser_STC5(String LastNAme,String FirstName , String phoneNumber,String ExternalContact) throws InterruptedException, IOException{
        logger=report.createTest("User_STC4", "Connect Admin");
        Admin_CreatUSer=new Admin_CreateUSer(driver, logger);
        Admin_CreatUSer.GetUseCaseDescription("User_STC4", 7, logger);
        Admin_CreatUSer.USerWithforwardexternal(LastNAme,FirstName,phoneNumber,ExternalContact);
    }

    @Parameters({"LastNAme","FirstName","phoneNumber","InternalContact"})
    @Test(priority = 20)
    void AdminUser_STC6(String LastNAme,String FirstName , String phoneNumber,String InternalContact) throws InterruptedException, IOException{
        logger=report.createTest("User_STC4", "Connect Admin");
        Admin_CreatUSer=new Admin_CreateUSer(driver, logger);
        Admin_CreatUSer.GetUseCaseDescription("User_STC4", 7, logger);
        Admin_CreatUSer.USerWithforwardcontact(LastNAme,FirstName,phoneNumber,InternalContact);
    }


    @Parameters({"LastNAme","FirstName","phoneNumber","CustomProfile"})
    @Test(priority = 22)
    void AdminUser_STC7(String LastNAme,String FirstName , String phoneNumber,String CustomProfile) throws InterruptedException, IOException{
        logger=report.createTest("User_STC7", "Connect Admin");
        Admin_CreatUSer=new Admin_CreateUSer(driver, logger);
        Admin_CreatUSer.GetUseCaseDescription("User_STC7", 7, logger);
        Admin_CreatUSer.USerWithBarring(LastNAme,FirstName,phoneNumber,CustomProfile);
    }

    @Parameters({"LastNAme","FirstName","phoneNumber","InternalContact","ExternalContact","CustomProfile"})
    @Test(priority = 22)
    void AdminUser_STC8(String LastNAme,String FirstName , String phoneNumber,String ExternalContact,String InternalContact,String CustomProfile) throws InterruptedException, IOException{
        logger=report.createTest("AdminUser_STC8", "Create Valid User from Web Admin ");
        Admin_CreatUSer=new Admin_CreateUSer(driver, logger);
        Admin_CreatUSer.GetUseCaseDescription("AdminUser_STC8", 7, logger);
        Admin_CreatUSer.CreatValideUser(LastNAme,FirstName,phoneNumber,ExternalContact,InternalContact,CustomProfile);
    }

    @Parameters({"ContactSearch"})
    @Test(priority = 22)
    void AdminUser_STC9(String ContactSearch) throws InterruptedException, IOException{
        logger=report.createTest("AdminUser_STC9", "Search User from Web Admin ");
        Admin_UpdateUSer=new Admin_UpdateUser(driver, logger);
        Admin_UpdateUSer.GetUseCaseDescription("AdminUser_STC9", 7, logger);
        Admin_UpdateUSer.SerchContact(ContactSearch);
    }



    @Parameters({"ContactSearch"})
    @Test(priority = 22)
    void AdminUser_STC10(String ContactSearch) throws InterruptedException, IOException{
        logger=report.createTest("AdminUser_STC10", "Edit User from Web Admin ");
        Admin_UpdateUSer=new Admin_UpdateUser(driver, logger);
        Admin_UpdateUSer.GetUseCaseDescription("AdminUser_STC10", 7, logger);
        Admin_UpdateUSer.DeactiveTransfer(ContactSearch);
    }

    @Parameters({"ContactSearch"})
    @Test(priority = 22)
    void AdminUser_STC11(String ContactSearch) throws InterruptedException, IOException{
        logger=report.createTest("AdminUser_STC10", "validate User from Web Admin ");
        Admin_UpdateUSer=new Admin_UpdateUser(driver, logger);
        Admin_UpdateUSer.GetUseCaseDescription("AdminUser_STC10", 7, logger);
        Admin_UpdateUSer.ValidationCode(ContactSearch);
    }

    @Parameters({"GroupName","User1","User2"})
    @Test(priority = 22)
    void AdminGroup_STC1(String GroupName,String user1,String user2) throws InterruptedException, IOException{
        logger=report.createTest("AdminGroup_STC1", "Create a group existant from Web Admin ");
        AdminCreateGroup=new Admin_CreatGroup(driver, logger);
        AdminCreateGroup.GetUseCaseDescription("AdminGroup_STC1", 7, logger);
        AdminCreateGroup.CreateGroup(GroupName,user1,user2);
    }


    @Parameters({"GroupName","User1","User2"})
    @Test(priority = 22)
    void AdminGroup_STC2(String GroupName,String user1,String user2) throws InterruptedException, IOException{
        logger=report.createTest("AdminGroup_STC1", "Create a group from Web Admin ");
        AdminCreateGroup=new Admin_CreatGroup(driver, logger);
        AdminCreateGroup.GetUseCaseDescription("AdminGroup_STC1", 7, logger);
        AdminCreateGroup.CreateGroup(GroupName,user1,user2);
    }

    @Parameters({"GroupName","User1"})
    @Test(priority = 22)
    void AdminGroup_STC3(String GroupName,String user1) throws InterruptedException, IOException{
        logger=report.createTest("AdminGroup_STC1", "Edit group from Web Admin ");
        AdminCreateGroup=new Admin_CreatGroup(driver, logger);
        AdminCreateGroup.GetUseCaseDescription("AdminGroup_STC1", 7, logger);
        AdminCreateGroup.EditGroup(GroupName,user1);
    }


    @Parameters({"GroupName"})
    @Test(priority = 22)
    void AdminGroup_STC4(String GroupName) throws InterruptedException, IOException{
        logger=report.createTest("AdminGroup_STC1", "Delete group from Web Admin ");
        AdminCreateGroup=new Admin_CreatGroup(driver, logger);
        AdminCreateGroup.GetUseCaseDescription("AdminGroup_STC1", 7, logger);
        AdminCreateGroup.Delete(GroupName);
    }


}
